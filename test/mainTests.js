const assert = require('chai').assert;
const main = require('../src/main');

//result
const hola_mundo =main.hola_mundo();
const resta = main.resta(7,5);

describe('test main', function () {
    
    describe('validaciones hola mundo', function () {
        it('validar el mensaje ', function(){
            assert.equal(hola_mundo, 'hola que hace', 'el texto no coincide');
        });
        it('validar tipo de variable', function(){
            assert.typeOf(hola_mundo, 'string', 'el tipo de variable no coincide');
        });
    });
    describe('validacion resta', function () {
        it('validar resultado ', function(){
            assert.equal(resta, 2, 'el resultado no ocincide');
        });
       
    });
    

});
